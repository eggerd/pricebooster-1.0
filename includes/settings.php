<?php

    $settings = array
    (
        // die Version unter der das Script aktuell läuft - wird lediglich im Debug-Log eingebunden
        'version' => '1.3.9',

        // Die nachfolgenden beiden Optionen enthalten die URLs zu den Profilen der beiden Tankstellen,
        // aus denen die Spritpreise ausgelesen werden sollen.
        // Wichtig: Diese URLs müssen auf ein Profil der Seite sparsamtanken.de verweiden, da die
        // Funktionen speziell für das HTML-Gerüst dieser Seite geschrieben wurden!
        'reference_source' => 'http://www.sparsamtanken.de/tankstellen-details?tankstelle=1',
        'own_source' => 'http://www.sparsamtanken.de/tankstellen-details?tankstelle=2',

        // Zeit in Sekunden die vergehen darf, bis die geladenen Preise als veraltet angesehen werden.
        // Im Falle von veralteten Preisen wird ein Fehler ausgegeben.
        'source_data_timeout' => 1800,

        // Der private URL zum Google Kalender, aus dem die Termine ermittelt werden sollten, die mit
        // der Tankstelle in Verbindung stehen. Das Script wertet alle Termine mit der Bezeichnung eines,
        // der hier definierten Mitarbeiter, als Arbeitszeit und wird während dieser Benachrichtigungen
        // versenden. Beispiel: "Dustin".
        // Dem URL werden zu einem späteren Zeitpunkt die Parameter start-min und start-max angehängt
        // um die Ausgabe auf den aktuellen Tag zu beschränken.
        'calendar' => 'http://www.google.com/calendar/feeds/97becqhe2uogcovbef.../basic',

        // Die verschiedenen Spritsorten und ihre zugehörigen Shortcuts (Format: shortcut => name)
        'fuels' => array('D' => 'Diesel', 'S' => 'Super E5', 'E' => 'Super E10'),

        // Hier kann die Reihenfolge festgelegt werden, in welcher die Spritsorten später aufgelistet
        // werden sollen. Hierzu wird einfach einem Shortcut eine Nummer zugeordnet, die dann für die
        // Position in der Liste steht (z.B. 'D' => 1 für Diesel an erster Stelle)
        'fuel_order' => array('P' => 1, 'D' => 2, 'S' => 3, 'E' => 4),

        // Der Absender der Benachrichtigung
        'submitter' => 'PriceBooster <pricebooster@example.com>',

        // Der Betreff der Benachrichtigung
        'subject' => 'Preissenkung',

        // Der Timeout in Sekunden, bevor das Script, nach der letzten Benachrichtigung, erneut die
        // Preise überprüft und ggf. erneut eine Benachrichtigung sendet
        'timeout' => 1800,

        // Die Datei in der das Script den Timeout-Timestamp speichert
        'timeout_file' => './data/timeout.info',

        // Die Datei in der das Script den die zuletzt übermittelten Preise speichert
        'prices_file' => './data/prices.info',

        // Der Timeout in Sekunden bis die gespeicherten Preise ungültig werden
        'prices_timeout' => 2700,

        // Überschreibt die vom Kalender vorgegebenen Arbeitszeiten und ermöglicht so z.B. das versenden von
        // Benachrichtigungen bevor die eigentliche Schicht begonnen hat - wie es bei der Morgenschicht nötig
        // ist. Der Syntax ist 'original Uhrzeit' => 'neue Uhrzeit' im Format H:MM
        'time_overwrite' => array('6:00' => '5:30', '7:00' => '6:30', '8:00' => '7:30', '22:00' => '21:00'),

        // Um wie viele Sekunden die Startzeit verringert werden soll, wenn diese zuvor noch nicht überschrieben wurde
        'default_start_overwrite' => 600,

        // Aktiviert den Protokoll-Output, durch den grafisch dargestellt wird welche Aktionen durchgeführt
        // und welche Werte verwendet wurden
        'debug' => true,

        // Wenn aktiviert wird das Script seinen Protokoll-Output in die Datei speichern, die unter
        // "debug_log_file" angegeben wurde
        'debug_log_output' => false,

        // In die hier definierte Datei wird der Protokoll-Output gespeichert, sollte "debug_native_saving"
        // aktiviert sein. Zudem wird die Größe dieser Datei überprüft, sollte der Wert von "debug_log_max_size"
        // größer als 0 sein
        'debug_log_file' => './data/debug.log',

        // Die maximale Größe der Log-Datei, in MB. Wird dieser Wert erreicht oder überschritten wird das Script
        // die Log-Datei leeren. Wird nur dann überprüft wenn "debug" aktiviert ist und der Wert größer als 0 ist
        'debug_log_max_size' => 5,

        // Gibt an ob der Status des Scriptes geloggt werden soll. Ist dies auf "true" wird in die Datei, die unter
        // "status_log_file" definiert wurde, der Erfolgs-Status des Scripts geloggt. Hierzu wird eine Fehler
         // Zahl oder "null" (für Erfolg) geloggt.
        'status_logging' => true,

        // Sollte bei der Ausführung des Scripts ein Fehler auftreten wird der nummerische Wert dieses Fehlers in
        // diese Datei geschrieben. Wird das Script ohne einen Fehler ausgeführt wird die Datei mit "NULL" gefüllt.
        'status_log_file' => './data/status.log',

        // Gibt an wie viele Fehler auftreten dürfen, bevor der Fehlercode geloggt wird und somit evtl. ein "Ausfall"
        // durch einen Status-Monitor festgestellt wird. Wird hier "0" definiert, werden die Fehler sofort erkannt.
        'error_tolerance_limit' => 3,

        // Zeit in Sekunden bis die gezählten Fehler ungültig sind und somit nicht mehr berücksichtigt werden. Der
        // Zähler wird beim Ablauf dieser Zeitspanne neu gestartet.
        'error_tolerance_timeout' => 1200,

        // In dieser Datei befindet sich der Zähler für aufgetretenen Fehler.
        'error_tolerance_file' => './data/errors.info',





        // +++ workaround +++
        // Super Plus kann bisher nicht ausgelesen werden. Daher wird Super Plus automatisch errechnet,
        // sollte sich der Preis von Super ändern. Der hier definierte Wert gibt an um wie viele Cents
        // Super Plus teurer als Super ist.
        'workaround_plus_price' => 4
    );

?>
