<?php

   /* Diese Funktion ließt die angegebene Quelle aus und ermittelt in deren Quellcode
    * die Preise der eingestellten Spritsorten. Die Preise der Spritsorten werden
    * anschließend als Array zurückgegeben. In diesem Array wird das Shortcut der
    * Spritsorte als Index verwendet und der Preis (als Integer) ist die Value mit
    * vorangehendem Shortcut (getrennt durch einen Punkt).
    *
    * Version:  1.1.5
    * Stand:    09. Dezember 2014
    *
    * Input:
    *   $source     : string    = der URL zur Webseite auf der die Preise zu finden sind
    *
    * Success-Output:
    *   array
    *   {
    *       [$shortcut :string] => $price :integer   // ['D'] => 138
    *   }
    *
    * Failure-Output:
    *   false   : boolean   = die Preise konnten nicht ermittelt werden
    */

    function fetch_fuel_prices($source)
    {
        global $settings;
                                                                                                                                             debug('source: '.$source, 1, 3);
        $return = array(); // wird später zurückgegeben
        $header = stream_context_create(array('http'=>array('user_agent'=>"User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11\r\n"))); // wird benötigt um den Quellcode auslesen zu können - Sparsamtanken.de hat eine Client-Only Restriction eingebaut!
        $website_source = @file_get_contents($settings[$source], false, $header); // Quellcode der Ziel-Webseite auslesen

        if($website_source !== false) // nur wenn der Quellcode ausgelesen werden konnte
        {
                                                                                                                                             debug('fetching defined prices', 1, 4);
            foreach($settings['fuels'] as $shortcut => $fuelname) // für jede eingestellte Spritsorte
            {
                preg_match('/'.$fuelname.'<\/a>:<\/td>(.*?)9 (.*?)vom(.*?)<\/td>/si', $website_source, $matches); // Preis inkl. Datum & Uhrzeit ermitteln
                //preg_match('/'.$fuelname.'<\/a>:<\/td>(.*?)9 /si', $website_source, $matches); // ermittelt das Preissegment

                // Zeitangeben voneinander trennen
                $data_timeinfo = preg_split('/ /', $matches[3]);
                $data_date = preg_split('/\./', $data_timeinfo[1]);
                $data_time = preg_split('/:/', $data_timeinfo[2]);

                if(mktime($data_time[0], $data_time[1], 0, $data_date[1], $data_date[0], $data_date[2]) >= time() - $settings['source_data_timeout']) // nur wenn Preise noch nicht zu alt sind
                {
                    if(isset($matches[1])) // nur wenn die Spritsorte inkl. Preis auch gefunden wurde
                    {
                        $price = preg_replace('/[^0-9]/', null, $matches[1]); // entfernt Leerzeichen und Kommas aus dem Preis
                                                                                                                                             debug($fuelname.' ('.$shortcut.') = '.$price, 1, 5);
                        $return[$shortcut] = intval($price); // speichert den Preis
                    }
                    else
                    {
                        trigger_error('couldn\'t find fuel information in external source code!');                                           debug('couldn\'t fetch "'.$fuelname.'" prices - abort', 1, 5);
                        return false; // Preisinformationen wurde nicht im Quellcode gefunden - Abbruch!
                    }
                }
                else
                {
                   trigger_error('fetched fuel information are outdated!');                                                                  debug('fetched "'.$fuelname.'" price is out of date - abort', 1, 5);
                   return false; // Preisinformationen sind zu alt - Abbruch! (wenn einer aus x Preisen outdated ist)
                }
            }

            return $return;
        }
        else
        {
            trigger_error('couldn\'t fetch fuel information from external source!');                                                         debug('couldn\'t fetch source code', 1, 4);
            return false; // Quellcode konnte nicht ausgelesen werden - Abbruch!
        }
    }





   /* Dies ist die primäre Funktion des Skripts. Wird diese Funktion aufgerufen
    * wird zudem die fetch_fuel_prices() Funktion aufgerufen um die Preise der eigenen
    * sowie der Referenz-Tankstelle zu ermitteln.
    * Anschließend vergleicht die Funktion die ermittelten Preise der beiden
    * Tankstellen. Preise die nicht übereinstimmen werden in einem Array gespeichert
    * und später zurückgegeben. Hierbei werden natürlich immer die Preise der Referenz-
    * Tankstelle verwendet.
    *
    * Version:  1.0.3
    * Stand:    22. April 2014
    *
    * Input:
    *   none
    *
    * Success-Output:
    *   // stimmen die Preise alle überein
    *   true    : boolean
    *
    *   // sollten die Preise nicht übereinstimmen
    *   array
    *   {
    *       [$shortcut :string] => $price :integer   // ['D'] => 138
    *   }
    *
    * Failure-Output:
    *   false   : boolean   = fetch_fuel_prices() konnte die Preise nicht ermitteln
    */

    function compare_prices()
    {
        global $settings;

        $return = array();

        /* Preise der beiden Tankstellen ermitteln */                                                                                        debug('fetching prices', 1, 2);
        $reference_prices = fetch_fuel_prices('reference_source');
        $own_prices = fetch_fuel_prices('own_source');

        if($reference_prices !== false && $own_prices !== false) // nur wenn die Preise erfolgreich ermittelt werden konnten
        {                                                                                                                                    debug('fetching saved prices', 1, 2);
            $saved_prices = fetch_saved_prices(); // zuletzt gesendete Preise auslesen
            //var_dump($saved_prices); echo '<br />';

            foreach($settings['fuels'] as $shortcut => $fuelname) // für jede eingestellte Spritsorte
            {                                                                                                                                debug('comparing: '.$fuelname. '('.$shortcut.')', 1, 2);
                if((array_key_exists($shortcut, $saved_prices) && $saved_prices[$shortcut] !== $reference_prices[$shortcut]) | !array_key_exists($shortcut, $saved_prices))
                {                                                                                                                            debug('saved price doesn\'t exist or is different', 1, 3);
                                                                                                                                             debug($own_prices[$shortcut].' <=> '.$reference_prices[$shortcut], 1, 3);
                    if($own_prices[$shortcut] > $reference_prices[$shortcut]) // wenn der Preis der Referenz-Tankstelle niedriger ist
                    {                                                                                                                        debug('new price - saved for return', 1, 3);
                        $return[$shortcut] = $reference_prices[$shortcut]; // Preis in Array speichern
                    }
                }
                else
                {
                    debug('same as saved price - no return', 1, 3);
                }
            }

            if(count($return) == 0) // ist das Array leer, stimmen alle Preise überein
            {                                                                                                                                debug('no new prices to return', 1, 2);
                return true;
            }
            else
            {                                                                                                                                debug('returning new prices', 1, 2);
                return $return;
            }
        }
        else // für eine von beiden Tankstellen konnten keine Preise ermittelt werden
        {                                                                                                                                    debug('canceling price comparison due to failure while fetching the fuel prices', 1, 2);
            return false; // Preisvergleich vorzeitig abbrechen
        }
    }





   /* Generiert aus dem übermitteltem Date-Time String zwei UNIX Timestamps - jeweils einen für
    * Start und Ende des Termins. Der String musst folgendes Format haben: XX 31 Dez 2013 16:00 XXX 17:00
    * Die X stehen für beliebige Zeichen, da diese Elemente nicht verwendet werden. Wichtig ist das
    * Einhalten der Leerzeichen da diese zur Spaltung der Elemente verwendet werden!
    *
    * Version:  1.1.1
    * Stand:    01. März 2014
    *
    * Input:
    *   $date_string    : string    = der Date-Time String (Format: XX 31 Dez 2013 16:00 XXX 17:00)
    *
    * Success-Output:
    *   array
    *   {
    *       ['start'] => integer    // Start-Timestamp
    *       ['end'] => integer    // End-Timestamp
    *   }
    *
    * Failure-Output:
    *   none
    */
    function get_timestamp($date_string)
    {
        global $settings;

        // Array um die Monate ihren numerischen Werten zuzuteilen
        $months = array('Jan' => 1, 'Feb' => 2, 'M&auml;r' => 3, 'Apr' => 4, 'Mai' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Okt' => 10, 'Nov' => 11, 'Dez' => 12);

        $date_string = preg_replace('/\./', null, $date_string); // entfernt alle Punkte aus dem String
        $segments = preg_split('/\s/', $date_string); /* teilt den String anhand der Leerzeichen in verschiedene Segmente */                 debug('splitting date string', 1, 3);
        /* 1 == tag */                                                                                                                       debug('day: '.$segments[1], 1, 4);
        /* 2 == monat string */                                                                                                              debug('month: '.$segments[2], 1, 4);
        /* 3 == jahr */                                                                                                                      debug('year: '.$segments[3], 1, 4);
        /* 4 == start zeit string */                                                                                                         debug('start: '.$segments[4], 1, 4);
        /* 6 == endzeit string */                                                                                                            debug('end: '.$segments[6], 1, 4);

        $start_time = preg_split('/:/', $segments[4]); // teilt die Uhrzeit anhand des Doppelpunktes in zwei Teile
        $end_time = preg_split('/:/', substr($segments[6], 0, 5)); // teil ebenfalls die Uhrzeit, begrenzt jedoch zunächst den String
                                                                   // auf 5 Zeichen, da hier normal zusätzliche Zeichen zu finden sind


        /* Start- oder Endzeit ggf. überschreiben */                                                                                         debug('checking for overwrite', 1, 3);
        $start_time_overwritten = false; // gibt später an, ob die Startzeit überschrieben wurde

        foreach($settings['time_overwrite'] as $time => $overwrite) // für jede Uhrzeit in den Settings
        {                                                                                                                                    debug('check for: '.$time.' => '.$overwrite, 1, 4);
            $time = preg_split('/:/', $time); // originale Zeit auftrennen
            $overwrite = preg_split('/:/', $overwrite); // neue Zeit auftrennen

            // wenn die Zeit mit der Zeit aus den Settings übereinstimmt (Start- oder Endzeit)
            if(intval($start_time[0]) == intval($time[0]) && intval($start_time[1]) == intval($time[1]))
            {                                                                                                                                debug('overwriting start time', 1, 5);
                // Startzeit überschreiben
                $start_time[0] = $overwrite[0];
                $start_time[1] = $overwrite[1];

                $start_time_overwritten = true;
            }
            elseif(intval($end_time[0]) == intval($time[0]) && intval($end_time[1]) == intval($time[1]))
            {                                                                                                                                debug('overwriting end time', 1, 5);
                // Endzeit überschreiben
                $end_time[0] = $overwrite[0];
                $end_time[1] = $overwrite[1];
            }
        }

        // Start- und End-Timestamp generieren
        $start_timestamp = mktime($start_time[0], $start_time[1], 0, $months[$segments[2]], $segments[1], $segments[3]);
        $end_timestamp = mktime($end_time[0], $end_time[1], 0, $months[$segments[2]], $segments[1], $segments[3]);

        if($start_time_overwritten == false) // wenn die Startzeit noch nicht überschrieben wurde
        {                                                                                                                                    debug('overwriting start time (default)', 1, 4);
            $start_timestamp -= $settings['default_start_overwrite']; /* Startzeit überschreiben */                                          debug($start_time[0].':'.$start_time[1].' => '.date('H:i', $start_timestamp), 1, 5);
        }

        return array('start' => $start_timestamp, 'end' => $end_timestamp);
    }





   /* Sendet eine E-Mail an die eingestellte E-Mail Adresse, welche die Preise der
    * Spritsorten enthält, die nicht übereingestimmt haben. Hierzu wird das Array übergeben
    * das durch compare_prices() erstellt wurde. Zudem ruft die Funktion set_timeout() auf
    * um einen Timeout mit der eingestellten Länge zu generieren.
    *
    * Version:  1.2.0
    * Stand:    19. Dezember 2013
    *
    * Input:
    *   $prices :array
    *   {
    *       [$shortcut :string] => $price :integer   // ['D'] => 138
    *   }
    *
    *   $employees :array
    *   {
    *       [] => :string   // E-Mail
    *   }
    *
    * Success-Output:
    *   none
    *   call->set_timeout()
    *
    * Failure-Output:
    *   none
    *   call->set_timeout()
    */
    function notify($prices, $employees)
    {
        global $settings;

        $message_cache = array(); // enthält später Bestandteile der Nachricht

        foreach($prices as $shortcut => $price)
        {
            $message_cache[$settings['fuel_order'][$shortcut]] = $shortcut." ".substr($price, 0, 1).",".substr($price, 1); // schreibt den Teil der Nachricht mit festgelegtem Key ins Array

            if($shortcut == 'S')
            {
                $plus_price = $price + $settings['workaround_plus_price'];
                $message_cache[$settings['fuel_order']['P']] = "P ".substr($plus_price, 0, 1).",".substr($plus_price, 1); // schreibt den Teil der Nachricht mit festgelegtem Key ins Array
            }
        }

        ksort($message_cache); // Array nach Keys sortieren
        $message = implode("\n", $message_cache);
        $receiver = implode(',', $employees);
                                                                                                                                             debug('message: '.$message, 1, 2);
                                                                                                                                             debug('receiver: '.$receiver, 1, 2);
        mail($receiver, $settings['subject'], $message, "From: ".$settings['submitter']);                                                    debug('mail submitted', 1, 2);
        set_timeout();
        set_prices($prices);
    }





   /* Ließt den aktuellen Timeout aus dem eingestellten Timeout-File aus und
    * gibt den Timeout-Timestamp zurück.
    *
    * Version:  1.0.0
    * Stand:    24. November 2013
    *
    * Input:
    *   none
    *
    * Success-Output:
    *   $timeout_content    : integer
    *
    * Failure-Output:
    *   false               : boolean
    */
    function fetch_timeout()
    {
        global $settings;

        if(is_readable($settings['timeout_file'])) // wenn die Datei lesbar ist
        {                                                                                                                                    debug('file is readable', 1, 2);
            $timeout_file = fopen($settings['timeout_file'], 'r'); // Datei öffnen
            $timeout_content = fread($timeout_file, filesize($settings['timeout_file'])); // Inhalt auslesen
                                                                                                                                             debug('selected timestamp: '.$timeout_content, 1, 2);
            return $timeout_content; // Inhalt (Timestamp) zurückgeben
        }
        else
        {                                                                                                                                    debug('file is not readable', 1, 2);
            return false;
        }
    }





   /* Setzt einen neuen Timeout-Timestamp in die eingestellte Timeout-Datei.
    *
    * Version:  1.0.0
    * Stand:    24. November 2013
    *
    * Input:
    *   none
    *
    * Success-Output:
    *   true    : boolean
    *
    * Failure-Output:
    *   false   : boolean
    */

    function set_timeout()
    {
        global $settings;
                                                                                                                                             debug('setting timeout', 1, 2);
        $timeout_file = fopen($settings['timeout_file'], 'w+'); // öffnet die eingestellte Timeout-Datei

        if(fwrite($timeout_file, time()) != false) // schreibt den Timestamp in die Datei
        {                                                                                                                                    debug('done', 1, 3);
            return true;
        }
        else
        {                                                                                                                                    debug('failure - file not writeable!', 1, 3);
            return false;
        }
    }





   /* Ließt die gespeicherten Preise aus der eingestellten Prices-Datei aus. Anhand des Timestamps, der
    * zusammen mit den Preisen gespeichert wird, wird entschieden ob diese Preise noch gültig sind.
    *
    * Version:  1.0.1
    * Stand:    09. Januar 2014
    *
    * Input:
    *   none
    *
    * Success-Output:
    *   array
    *   {
    *       [$shortcut :string] => $price :integer   // ['D'] => 138    bzw. einmalig ['timestamp'] => 0123456789
    *   }
    *
    * Failure-Output:
    *   false   : boolean
    */
    function fetch_saved_prices()
    {
        global $settings;

        if(is_readable($settings['prices_file'])) // wenn die Datei lesbar ist
        {                                                                                                                                    debug('file is readable', 1, 3);
            $prices_file = fopen($settings['prices_file'], 'r'); // Datei öffnen
            $prices_content = fread($prices_file, filesize($settings['prices_file'])); // Inhalt auslesen
                                                                                                                                             debug('content: '.$prices_content, 1, 3);
            $prices = array();
            $prices_content = preg_split('/;/', $prices_content); // Daten auftrennen

            foreach($prices_content as $value) // für alle vorhandenen Daten
            {
                $data = preg_split('/\./', $value); // Index von Value trennen

                if($data[0] == 'timestamp') // wenn der Index 'timestamp' ist
                {                                                                                                                            debug('timestamp: '.$data[1], 1, 3);
                    if(intval($data[1]) + $settings['prices_timeout'] < time()) // prüfen ob der Timeout schon erreicht wurde
                    {                                                                                                                        debug('timeout expired - break!', 1, 3);
                        break; // Timeout erreicht - Schleife unterbrochen bevor die Preise verarbeitet werden konnten
                    }
                }
                else
                {                                                                                                                            debug($data[0].' = '.$data[1], 1, 3);
                    $prices[$data[0]] = intval($data[1]); // Preis speichern
                }
            }

            return $prices; // Preise als Array zurückgeben
        }
        else
        {
            return false;
        }
    }





   /* Speichert die übergebenen Preise in die Prices-Datei. Hierbei werden die alten Daten zuvor ausgelesen
    * und ggf. von den neuen Daten überschrieben.
    *
    * Version:  1.0.1
    * Stand:    09. Januar 2014
    *
    * Input:
    *   array
    *   {
    *       [$shortcut :string] => $price :integer   // ['D'] => 138
    *   }
    *
    * Success-Output:
    *   true    : boolean
    *
    * Failure-Output:
    *   false   : boolean
    */
    function set_prices($prices)
    {                                                                                                                                        debug('saving prices', 1, 2);
        global $settings;
                                                                                                                                             debug('fetching current content', 1, 2);
        $saved_prices = fetch_saved_prices(); // alte Preise aus der Info-Datei auslesen
        $price_strings = array();

        /* alte und neue Daten zusammenfügen */                                                                                              debug('constructing data', 1, 2);
        foreach($settings['fuels'] as $shortcut => $name)
        {                                                                                                                                    debug($name.' ('.$shortcut.')', 1, 3);
            if(array_key_exists($shortcut, $prices)) // wenn die neuen Preise Daten zum aktuellen Shortcut enthalten
            {                                                                                                                                debug('saving new price: '.$prices[$shortcut], 1, 4);
                $data[$shortcut] = $prices[$shortcut]; // den neuen Preis speichern
            }
            else // die neuen Preise enthalten keine Daten zum aktuellen Shortcut
            {
                if(array_key_exists($shortcut, $saved_prices)) // nur wenn der gespeicherte Preis auch übergeben wurde bzw. vorhanden ist
                {                                                                                                                            debug('keeping old price: '.$saved_prices[$shortcut], 1, 4);
                    $data[$shortcut] = $saved_prices[$shortcut]; // zuletzt übermittelten Preis speichern
                }
                else
                {
                    debug('not contained', 1, 3);
                }
            }
        }

        foreach($data as $shortcut => $price) // Shortcuts und Preise zusammenfügen
        {
            $price_strings[] = $shortcut.'.'.$price;
        }

        $content = 'timestamp.'.time().';'.implode(';', $price_strings); // Timeout-Timestamp und Preis-Strings zusammenfügen
                                                                                                                                             debug('new content: '.$content, 1, 2);

        $prices_file = fopen($settings['prices_file'], 'w+'); // öffnet die eingestellte Prices-Datei
                                                                                                                                             debug('saving new content', 1, 2);
        if(fwrite($prices_file, $content) != false) // schreibt die Daten in die Datei
        {                                                                                                                                    debug('done', 1, 3);
            return true;
        }
        else
        {                                                                                                                                    debug('failure - file not writeable', 1, 3);
            return false;
        }
    }





   /* An diese Funktion werden Strings übergeben, die anschließend ausgegeben werden. Zuvor
    * wird dem String eine Reihe von Bindestrichen vorrangesetzt um ein Einrücken zu simulieren.
    * Die Funktion kann somit für die Ordnung von Output sorgen (einrücken), Zeilenumbrücke
    * erzeugen und ggf. den Output dumpen.
    *
    * Version:  1.0.0
    * Stand:    09. Januar 2013
    *
    * Input:
    *   $string     : string    = der Text String der ausgegeben wird
    *   $line_break : integer   = die Anzahl der Zeilenumbrüche die generiert werden sollen
    *   $intend     : integer   = die Anzahl der Bindestrich-Segmente
    *   $dumping    : boolean   = ob der String gedumpt weden soll
    *
    * Success-Output:
    *   none
    *
    * Failure-Output:
    *   none
    */
    function debug($string, $line_break = 1, $intend = 1, $dumping = false)
    {
        global $settings;

        if($settings['debug'] == true) // nur wenn Debug in den Settings aktiv ist
        {
            $lines = null;
            $breaks = null;

            for($i = 1; $i <= $intend; $i++) // gewünschte Anzahl an Bindestrichen generieren
            {
                $lines .= '--';
            }

            for($i = 1; $i <= $line_break; $i++) // gewünschte Anzahl an Zeilenumbrüchen generieren
            {
                $breaks .= '<br />';
            }


            if($dumping == false)
            {
                echo $lines.'> '.$string.$breaks; // String ausgeben
            }
            else
            {
                var_dump($string); // String dumpen
            }
        }
    }





   /* Hat die Log-Datei eine bestimmte Größe erreicht, die in den Settings festgelegt werden
    * kann, wird diese Funktion die Log-Datei leeren und einen Vermerk am Anfang der Datei
    * einfügen.
    *
    * Version:  1.0.0
    * Stand:    04. März 2014
    *
    * Input:
    *   none
    *
    * Success:
    *   true    : boolean
    *
    * Failure:
    *   false   : boolean
    */
    function flush_debug_log()
    {
        global $settings;

        if($settings['debug'] === true)
        {
            if(!empty($settings['debug_log_file'])) // nur wenn eine Log-Datei definiert wurde
            {
                $debug_file_size = filesize($settings['debug_log_file']); // Dateigröße in Bytes
                $debug_file_size = ($debug_file_size / 1024) / 1024; // Dateigröße in MB umrechnen

                if($settings['debug_log_max_size'] > 0 && $debug_file_size >= $settings['debug_log_max_size']) // wenn die Dateigröße die angegebene Größe erreicht hat
                {
                    $debug_file = fopen($settings['debug_log_file'], "w"); // der "W"-Modus leert die Datei
                    fwrite($debug_file, '***** FILE FLUSHED @ '.date('d.m.y - H:i', time()).' *****<br /><br /><br />');
                    fclose($debug_file);

                    return true;
                }
                else
                {
                    return false; // maximale Größe noch nicht erreicht oder nicht definiert
                }
            }
            else
            {
                return false; // es wurde keine Log-Datei definiert
            }
        }
        else
        {
            return false; // Debugging ist deaktiviert
        }
    }





   /* Diese Funktion sollte als aller letzte Funktion im Script aufgerufen werden, da diese
    * den Erfolgs-Status des Scriptes in eine Datei loggt. Hierzu wird überprüft ob während
    * des Ausführens Fehlermeldungen aufgetreten sind. Sollte dies der Fall sein wird der
    * nummerische Wert der Fehlermeldung in die Datei geschrieben. Bei einem Durchlauf ohne
    * Fehler wird "null" in die Datei geschrieben.
    *
    * Version:  1.1.0
    * Stand:    17. August 2014
    *
    * Input:
    *   none
    *
    * Success:
    *   true    : boolean
    *
    * Failure:
    *   false   : boolean
    */
    function log_status()
    {
        global $settings;

        if(!empty($settings['status_log_file'])) // nur wenn eine Log-Datei definiert wurde
        {
            if($settings['status_logging'] === true) // wenn Status-Logging aktiviert ist
            {
                if(is_array(error_get_last())) // wenn die Funktion ein Array mit der letzten Fehlermeldung zurückgibt
                {
                    $error_type = error_get_last()['type']; // nummerischer Wert der Fehlermeldung speichern

                    if(error_tolerance() == false) // sollte der Fehler noch innerhalb der Toleranz liegen, wird dieser ignoriert!
                    {
                        $error_type = 'null';
                    }
                }
                else
                {
                    $error_type = 'null';
                }
            }
            else
            {
                // Status-Logging ist nicht aktiviert - dennoch wird die Datei immer wieder mit "null" gefüllt
                // um mögliche Komplikationen mit anderen Anwendungen, die diese Datei auswerten, zu verhindern.
                $error_type = 'null';
            }

            $status_file = fopen($settings['status_log_file'], "w"); // der "W"-Modus leert die Datei
            fwrite($status_file, $error_type); // Wert in die Datei schreiben
            fclose($status_file);

            return true;
        }
        else
        {
            return false; // es wurde keine Log-Datei definiert
        }
    }





   /* Da der Monit-Check nicht so angepasst werden kann, dass erst nach drei Fehlern ein
    * Alarm ausgelöst wir, übernimmt dies der PriceBooster. Hierzu zählt der PriceBooster
    * einen Counter hoch, sollte ein Fehler auftreten. Erreicht dieser Counter, innerhalb
    * einer definierten Zeitspanne, das definierte Limit, so wird der Fehlercode druch die
    * log_status() Funktion gespeichert - wodurch der Monit-Check einen Fehler feststellt.
    *
    * Version:  1.0.0
    * Stand:    17. August 2014
    *
    * Input:
    *   none
    *
    * Success:
    *   true    : boolean   // wird zurückgegeben wenn das Limit erreicht und der Fehler ausgewertet werden soll
    *
    * Failure:
    *   false   : boolean   // wird zurückgegeben wenn das Limit bisher nicht erreicht wurde
    */
    function error_tolerance()
    {
        global $settings;

        if(!empty($settings['error_tolerance_file'])) // nur wenn eine Log-Datei definiert wurde
        {
            $tolerance_file = fopen($settings['error_tolerance_file'], 'r+'); // Fehlertoleranz-Datei öffnen

            $tolerance_content = fread($tolerance_file, filesize($settings['error_tolerance_file'])); // Inhalt auslesen
            $tolerance_content = preg_split('/;/', $tolerance_content); // Inhalt, der durch Semikolon getrennt ist, aufspalten

            fclose($tolerance_file); // Datei wieder schließen
            $tolerance_file = fopen($settings['error_tolerance_file'], 'w'); // Fehlertoleranz-Datei öffnen, aber diesmal dabei leeren

            if($tolerance_content[0] + $settings['error_tolerance_timeout'] >= time()) // wenn der Counter noch nicht abgelaufen ist
            {
                $count = $tolerance_content[1] + 1; // Counter erhöhen

                fwrite($tolerance_file, time().';'.$count); // Counter mit neuem Timestamp speichern
                fclose($tolerance_file); // Datei wieder schließen

                if($count >= $settings['error_tolerance_limit']) // wenn der Counter das Limit erreicht hat
                {
                    return true;
                }
            }
            else // Timestamp ist abgelaufen
            {
                fwrite($tolerance_file, time().';1'); // Counter (=1) mit neuem Timestamp speichern
                fclose($tolerance_file); // Datei wieder schließen

                if($settings['error_tolerance_limit'] <= 0) // sollte es kein wahres Limit gibt
                {
                    return true; // da kein Limit existiert, wurde das Limit definitiv erreicht
                }
            }

            return false; // das Limit wurde bisher nicht erreicht
        }
        else
        {
            return true; // es wurde keine Log-Datei definiert - Fehler direkt auswerten
        }
    }

?>
