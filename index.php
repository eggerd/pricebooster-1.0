<?php

    require_once('./includes/settings.php');
    require_once('./includes/users.php');
    require_once('./includes/functions.php');

    // Wenn Debug aktiviert, Output-Logging aktiviert und eine Log-Datei definiert sind, wird der Output zwischengespeichert
    if($settings['debug'] === true && $settings['debug_log_output'] === true && !empty($settings['debug_log_file']))
    {
        ob_start(); // Output buffern
    }
                                                                                                                                             debug('start of script [v'.$settings['version'].'] @ '.date('d.m.y - H:i', time()));
                                                                                                                                             debug('fetching timeout from file');
    $timeout = fetch_timeout(); // ließt den aktuellen Timeout aus

    if($timeout !== false && $timeout + $settings['timeout'] <= time()) // wenn der Timeout abgelaufen ist
    {                                                                                                                                        debug('timeout expired');
        $date = date('Y-m-d', time()); // wird in den Kalender URL eingebaut um die Ausgabe zu begrenzen

        // XML-Daten des Google Calenders auslesen - begrenzt auf den aktuellen Tag
        $calendar = file_get_contents($settings['calendar'].'?start-min='.$date.'T00:00:00&start-max='.$date.'T23:59:59');

        if($calendar !== false) // wenn der Content geladen werden konnte (Server senden Antwort)
        {
            $calendar_xml = simplexml_load_string($calendar);
                                                                                                                                             debug('fetched calendar data');
            $working = array(); // Variable initialisieren
                                                                                                                                             debug('searching for current workshift');
            foreach($calendar_xml->entry as $event) // für jeden Termin im Kalender
            {
                preg_match('/^Wann:\s(.*?)\sMES?Z/is', $event->summary, $date_string); /* ließt das Datum und die Uhrzeit als String aus */  debug('time string: '.$date_string[1], 1, 2);
                                                                                                                                             debug('generating timestamps:', 1, 2);
                $times = get_timestamp($date_string[1]); /* generiert aus dem Zeit-Datums String zwei gültige Timestamps (Start & Ende) */
                                                                                                                                             debug('start: '.$times['start'], 1, 3);
                                                                                                                                             debug('end: '.$times['end'], 1, 3);
                if(time() >= $times['start'] && time() < $times['end']) // wenn die aktuelle Zeit zwischen dem Start- und End-Timestamp liegt
                {
                    $employee = strval($event->title); /* Event-Name und damit den Name des arbeitenden Mitarbeiters, als String, speichern */   debug('active workshift: '.$employee, 1, 2);
                                                                                                                                             debug('checking employee', 1, 2);
                    if(array_key_exists($employee, $users)) // nur dann, wenn der arbeitende in den Settings definiert wurde
                    {                                                                                                                        debug('registered', 1, 3);
                        $working[] = $users[$employee]; // Mitarbeiter als arbeitend eintragen
                    }
                }
            }
                                                                                                                                             debug('end of workshift check');

            if(count($working) > 0) // nur wenn gerade Mitarbeiter arbeiten, die für Boosts eingetragen wurden
            {                                                                                                                                debug('comparing prices');
                $price_data = compare_prices(); // vergleicht die Preise der beiden Tankstellen und gibt das Ergebnis zurück

                // nur wenn kein "true" (Preise unterscheiden sich nicht) und kein "false" (Fehler beim auslesen der Preise) kommt
                if($price_data !== true && $price_data !== false)
                {                                                                                                                            debug('notifying employees');
                    notify($price_data, $working); // E-Mail versenden
                }
            }
        }
        else
        {
            trigger_error('couldn\'t fetch calendar data - service unavailable!');                                                           debug('couldn\'t fetch calendar data - service unavailable!');
        }
    }
    else // Timeout ist noch aktiv
    {
        debug('active timeout: '.$timeout);
    }



                                                                                                                                             debug('end of script', 5);

    flush_debug_log(); // Größe der Log-Datei überprüfen und ggf. leeren

    // den Zwischengespeicherten Output nun am Ende der Log-Datei einfügen
    if($settings['debug'] === true && $settings['debug_log_output'] === true && !empty($settings['debug_log_file']))
    {
        file_put_contents($settings['debug_log_file'], ob_get_contents(), FILE_APPEND); // Output anhängen
        ob_end_flush(); // Buffering beenden und Output ausgeben
    }

    log_status(); // schreibt den Script Status in eine Datei

?>
