# PriceBooster 1.0 

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F23701663%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/pricebooster-1.0/-/releases) 

Mit dem PriceBooster können sich Mitarbeiter einer Tankstelle über Preissenkungen einer konkurrierenden Tankstelle informieren lassen, um so schnellstmöglich nachziehen zu können.

- Regelmäßige Überprüfung der Preise eines Wettbewerbpartners
- Benachrichtigung des Personals bei Preissenkungen via E-Mail
- Nur schichthabendes Personal erhält Benachrichtigungen

Von November 2013 bis Oktober 2015 war der PriceBooster 1.0 erfolgreich an einer Tankstelle im Einsatz, wurde jedoch schließlich von [Version 2.0](https://gitlab.com/eggerd/PriceBooster-2.0) abgelöst.

## Voraussetzungen

- PHP Version 5.X
- Die Möglichkeit Cronjobs zu erstellen
- Ein eigener Google Kalender in dem die Schichten eingetragen werden

## Installation

Zunächst sollte die Konfigurationsdatei in `/includes/settings.php` den eigenen Bedürfnissen angepasst werden. Mindestens müssen dort jedoch die Quellen für die eigene und die konkurrierende Tankstelle sowie der für die Schichten zu verwendende Google Kalender eingetragen werden.

Anschließend müssen die Namen des Personals und deren E-Mail Adressen in `/includes/users.php` eingetragen werden. Die dort verwendeten Namen des Personals müssen später im konfigurierten Google Kalender als Event-Titel verwendet werden, um eine Schicht dem korrekten Mitarbeiter zuordnen zu können. 

Um den PriceBooster auszuführen, muss schließlich noch ein Cronjob wie z.B. der nachfolgende konfiguriert werden. Es wird empfohlen, den PriceBooster nicht öfter als alle fünf Minuten auszuführen.
```
*/5 *5-21* * * * php5-cgi -q /path_to_script/index.php >> /path_to_script/data/debug.log 2>&1
```

## Lizenzierung

Copyright (c) 2015 Dustin Eckhardt. Alle Rechte vorbehalten.
